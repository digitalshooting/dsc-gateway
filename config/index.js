const fs = require('fs');

let config;

const DSCGatewayConfig = process.env["DSCGatewayConfig"];
if (DSCGatewayConfig != null) {
	let rawdata = fs.readFileSync(DSCGatewayConfig);
	config = JSON.parse(rawdata);
}
else {
	config = {
		lines: require("./lines.js"),
		network: require("./network.js"),
		database: require("./database.js"),
		permissions: require("./permissions.js"),
		relay: require("./relay.js"),
	};
}

module.exports = config;
