FROM node:15-buster

WORKDIR /usr/src/dsc-gateway

#RUN apt update
#RUN apt install -y g++

COPY package*.json ./

COPY . .

RUN npm install

EXPOSE 4000
CMD [ "node", "index.js" ]
